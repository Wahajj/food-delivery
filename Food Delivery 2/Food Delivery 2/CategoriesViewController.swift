//
//  ViewController.swift
//  Food Delivery 2
//
//  Created by Muhammad Wahaj on 6/19/20.
//  Copyright © 2020 Muhammad Wahaj. All rights reserved.
//

import UIKit

class CategoriesViewController: UITableViewController {
 
    var catItems = [CategoriesItem]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let item = CategoriesItem()
        item.name = "Fast Food"
        catItems.append(item)
        
        let item1 = CategoriesItem()
        item1.name = "Desi"
        catItems.append(item1)
        
        let item2 = CategoriesItem()
        item2.name = "Cafe"
        catItems.append(item2)
        
        let item3 = CategoriesItem()
        item3.name = "Deserts & Bakes"
        catItems.append(item3)
        
        for restaurants in catItems
        {
            if restaurants.name == "Fast Food"{
                let res = RestaurantItem()
                res.name = "Thrill Grill"
                restaurants.resItem.append(res)
                
                let res1 = RestaurantItem()
                res1.name = "Burger Lab"
                restaurants.resItem.append(res1)
                
                let res2 = RestaurantItem()
                res2.name = "Broadway"
                restaurants.resItem.append(res2)
                
                let res3 = RestaurantItem()
                res3.name = "Moos n Clucks"
                restaurants.resItem.append(res3)
                
                let res4 = RestaurantItem()
                res4.name = "Oh My Grill"
                restaurants.resItem.append(res4)
                
            } else if restaurants.name == "Desi"{
                let res = RestaurantItem()
                res.name = "Urban Tarka"
                restaurants.resItem.append(res)
                
                let res1 = RestaurantItem()
                res1.name = "Student Biryani"
                restaurants.resItem.append(res1)
                
                let res2 = RestaurantItem()
                res2.name = "Shaheen Shinwari"
                restaurants.resItem.append(res2)
                
                let res3 = RestaurantItem()
                res3.name = "Kababjees"
                restaurants.resItem.append(res3)
                
                let res4 = RestaurantItem()
                res4.name = "Hot n Spicy"
                restaurants.resItem.append(res4)
                
            }else if restaurants.name == "Cafe"{
                let res = RestaurantItem()
                res.name = "Koffie Chalet"
                restaurants.resItem.append(res)
                
                let res1 = RestaurantItem()
                res1.name = "New York Cafe"
                restaurants.resItem.append(res1)
                
                let res2 = RestaurantItem()
                res2.name = "Dunkin Donuts"
                restaurants.resItem.append(res2)
                
                let res3 = RestaurantItem()
                res3.name = "Gloria Jeans"
                restaurants.resItem.append(res3)
                
            } else if restaurants.name == "Deserts & Bakes"{
                let res = RestaurantItem()
                res.name = "Kababjees Bakers"
                restaurants.resItem.append(res)
                
                let res1 = RestaurantItem()
                res1.name = "Delizia"
                restaurants.resItem.append(res1)
                
                let res2 = RestaurantItem()
                res2.name = "Del Frio"
                restaurants.resItem.append(res2)
                
                let res3 = RestaurantItem()
                res3.name = "Hob Nob"
                restaurants.resItem.append(res3)
                
                let res4 = RestaurantItem()
                res4.name = "Blue Ribbon"
                restaurants.resItem.append(res4)
            }
        }
    
  
       
       // print(catItems[0].resItem[0].name)
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return catItems.count
      }


      override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier" , for: indexPath) as! CategoriesTableViewCell
         
        let item = catItems[indexPath.row]
     //   print(item.name)
        cell.categories = item
        configureText(for: cell, with: item)

     

          return cell
      }
    
    
    func configureText (for cell: UITableViewCell, with item: CategoriesItem)
    {
        let label = cell.viewWithTag(1000) as! UILabel
        label.text = item.name
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let categories = catItems[indexPath.row]
  //      print(categories.name)
//        print(categories.resItem[0].name)
        performSegue(withIdentifier: "ShowRes", sender: categories)
    }
    
   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
          if segue.identifier == "ShowRes" {
            let controller = segue.destination as! RestaurantsTableViewController
            controller.categories = sender as? CategoriesItem
            //controller.name = "wahaj"
            //print(controller.categories.name)
            //print(controller.categories.resItem[0].name)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    

    


}

