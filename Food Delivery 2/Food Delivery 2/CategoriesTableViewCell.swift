//
//  CategoriesTableViewCell.swift
//  Food Delivery 2
//
//  Created by Muhammad Wahaj on 6/29/20.
//  Copyright © 2020 Muhammad Wahaj. All rights reserved.
//

import UIKit

class CategoriesTableViewCell: UITableViewCell {

    @IBOutlet weak var CategoriesImageView: UIImageView!
    
    var categories: CategoriesItem! {
        didSet{
            updateUI()
        }
    }
    
    func updateUI(){
        CategoriesImageView.image = UIImage(named: categories.name)
    }
}
