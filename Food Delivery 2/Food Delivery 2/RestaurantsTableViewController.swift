//
//  RestaurantsTableViewController.swift
//  Food Delivery 2
//
//  Created by Muhammad Wahaj on 6/21/20.
//  Copyright © 2020 Muhammad Wahaj. All rights reserved.
//

import UIKit

class RestaurantsTableViewController: UITableViewController {
    
    var categories: CategoriesItem!
    var name = ""

    override func viewDidLoad() {
        super.viewDidLoad()
  //      print(categories?.name ?? 0)
        print(name)

    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {

        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

      return categories.resItem.count
        //return 1
    }

    
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier" , for: indexPath) as! RestaurantsTableViewCell
       
        let item = categories.resItem[indexPath.row]
        cell.restaurant = item
        configureText(for: cell, with: item)
        return cell
    }
  
  
  func configureText (for cell: UITableViewCell,
                      with item: RestaurantItem){
      
      let label = cell.viewWithTag(1001) as! UILabel
    label.text = item.name
  }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let restaurant = categories.resItem[indexPath.row]
        //print(restaurant.name)
        performSegue(withIdentifier: "ShowMenu", sender: restaurant)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowMenu"{
            let controller = segue.destination as! MenuTableViewController
            controller.Restaurant = sender as? RestaurantItem
            print(controller.Restaurant.name)
        }
    }
    
}
