//
//  RestaurantsTableViewCell.swift
//  Food Delivery 2
//
//  Created by Muhammad Wahaj on 6/30/20.
//  Copyright © 2020 Muhammad Wahaj. All rights reserved.
//

import UIKit

class RestaurantsTableViewCell: UITableViewCell {

    @IBOutlet weak var RestaurantImageView: UIImageView!
    var restaurant: RestaurantItem!{
        didSet{
            updateUI()
        }
    }
    
    func updateUI(){
        RestaurantImageView.image = UIImage(named: restaurant.name)
    }

}
