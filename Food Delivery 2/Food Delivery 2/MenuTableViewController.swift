//
//  MenuTableViewController.swift
//  Food Delivery 2
//
//  Created by Muhammad Wahaj on 7/3/20.
//  Copyright © 2020 Muhammad Wahaj. All rights reserved.
//

import UIKit

class MenuTableViewController: UITableViewController {
    var Restaurant: RestaurantItem!
    var BurgerLab = [DataModel]()
    var Delizia = [DataModel]()
    var allMenu = [String : [DataModel]]()
  
    override func viewDidLoad() {
        super.viewDidLoad()
        //print(Restaurant.name)
        let Burger1 = DataModel()
        Burger1.menu = "Beef Cheddar"
        Burger1.price = 350
        BurgerLab.append(Burger1)
        
        let Burger2 = DataModel()
        Burger2.menu = "Melt away"
        Burger2.price = 380
        BurgerLab.append(Burger2)
        
        let Burger3 = DataModel()
        Burger3.menu = "Crunchos"
        Burger3.price = 300
        BurgerLab.append(Burger3)
        
        let Burger4 = DataModel()
        Burger4.menu = "Messy Meat"
        Burger4.price = 380
        BurgerLab.append(Burger4)
        
        let cake1 = DataModel()
        cake1.menu = "Choclate Fudge"
        cake1.price = 800
        Delizia.append(cake1)
        
        let cake2 = DataModel()
        cake2.menu = "Red Valvet"
        cake2.price = 900
        Delizia.append(cake2)
        
        let cake3 = DataModel()
        cake3.menu = "Nutella"
        cake3.price = 850
        Delizia.append(cake3)
        
        let cake4 = DataModel()
        cake4.menu = "Dark Choclate"
        cake4.price = 1000
        Delizia.append(cake4)
        
        allMenu["Burger Lab"] = BurgerLab
        allMenu["Delizia"] = Delizia
        //let value = allMenu[Restaurant.name]
        //print (value?.count)
        

    }
    

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let value = allMenu[Restaurant.name]!
        return value.count
        
    }
    
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier" , for: indexPath)
        
        let Res = allMenu[Restaurant.name]!
        let item = Res[indexPath.row]
        configureText (for: cell, with: item)
       
         return cell
     }
    
     func configureText (for cell: UITableViewCell, with item: DataModel){
        
        let label = cell.viewWithTag(1002)as! UILabel
        label.text = item.menu
        let label2 = cell.viewWithTag(1003) as! UILabel
        label2.text = String(item.price) + "Rs"
    }
    
       override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return 81
       }
    
    
       
        


}
